package typeclientbundle

type TypeClient struct {
	ID   int    `json:"id"`
	Type string `json:"type" gorm:"UNIQUE"`
}
