package typeclientbundle

import (
	"net/http"
	"strconv"

	"bitbucket.org/gab_goffart/gesclient/bundles/basebundle"
	"bitbucket.org/gab_goffart/gesclient/store"
)

//TypeClientController controller for the TypeClient model
type TypeClientController struct {
	basebundle.BaseController
}

//GetAll Retrieves all the TypeClients in database and sends them in the response in JSON
func (t *TypeClientController) GetAll(res http.ResponseWriter, req *http.Request) {
	db := store.GetDB()

	list := []TypeClient{}

	db.Find(&list)

	t.SendJSON(res, req, list)
}

//GetByID Retrieves one TypeClient and sends it to
func (t *TypeClientController) GetByID(res http.ResponseWriter, req *http.Request) {

	db := store.GetDB()

	urlParam := t.ParseURL(req, "id")

	param, err := strconv.ParseInt(urlParam, 10, 64)

	if err != nil {
		panic("Error parsing the url string " + urlParam)
	}

	typeClient := TypeClient{}

	db.First(&typeClient, TypeClient{ID: int(param)})

	t.SendJSON(res, req, typeClient)
}
