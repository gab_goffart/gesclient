package statusclientbundle

import (
	"net/http"
	"strconv"

	"bitbucket.org/gab_goffart/gesclient/bundles/basebundle"
	"bitbucket.org/gab_goffart/gesclient/bundles/typeclientbundle"
	"bitbucket.org/gab_goffart/gesclient/store"
	"github.com/gorilla/mux"
)

type StatusClientController struct {
	basebundle.BaseController
}

func (s *StatusClientController) GetByTypeClient(res http.ResponseWriter, req *http.Request) {

	db := store.GetDB().Set("gorm:auto_preload", true)

	urlParam := s.BaseController.ParseURL(req, "id")

	param, err := strconv.ParseInt(urlParam, 10, 64)

	if err != nil {
		panic("Error parsing the string")
	}

	list := []StatusClient{}

	id := int(param)
	//I have typeclient ID, i want StatusClient
	db.Model(typeclientbundle.TypeClient{ID: id}).Related(&list)

	defer db.Close()

	if len(list) == 0 {
		s.SendJSON(res, req, nil)
	} else {
		s.SendJSON(res, req, &list)
	}

}

func (s *StatusClientController) GetByID(res http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	param, err := strconv.ParseInt(vars["id"], 10, 64)

	if err != nil {
		panic("Error parsing the string\n" + err.Error())
	}

	id := int(param)

	status := StatusClient{}

	db := store.GetDB()

	db.First(&status, &StatusClient{ID: id})

	defer db.Close()

	if status.ID == 0 {
		s.BaseController.SendJSON(res, req, nil)
	} else {
		s.BaseController.SendJSON(res, req, status)
	}

}
