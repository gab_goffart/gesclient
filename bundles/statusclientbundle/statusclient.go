package statusclientbundle

import "bitbucket.org/gab_goffart/gesclient/bundles/typeclientbundle"

type StatusClient struct {
	ID           int                         `json:"id"`
	Status       string                      `json:"status" gorm:"UNIQUE"`
	TypeClientID int                         `json:"typeclientid"`
	Type         typeclientbundle.TypeClient `json:"type" gorm:"foreignkey:TypeClientID"`
}
