package clientbundle

import (
	"time"

	"bitbucket.org/gab_goffart/gesclient/bundles/statusclientbundle"
	"bitbucket.org/gab_goffart/gesclient/bundles/typeclientbundle"
)

type Client struct {
	ID             int
	Nom            string                          `json:"nom"`
	Prenom         string                          `json:"prenom"`
	Courriel       string                          `json:"courriel"`
	Telephone      string                          `json:"telephone"`
	Adresse        string                          `json:"adresse"`
	Commentaire    string                          `json:"commentaire"`
	Frequence      int                             `json:"frequence"`
	Suivi          time.Time                       `json:"suivi"`
	Status         statusclientbundle.StatusClient `json:"status" gorm:"foreignkey:StatusClientID"`
	StatusClientID int                             `json:"statusclientid"`
	Type           typeclientbundle.TypeClient     `json:"type" gorm:"foreignkey:TypeClientID"`
	TypeClientID   int                             `json:"typeclientid"`
}
