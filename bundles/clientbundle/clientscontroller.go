package clientbundle

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/gab_goffart/gesclient/bundles/basebundle"
	"bitbucket.org/gab_goffart/gesclient/store"
)

//ClientController Controller for the Client model
type ClientController struct {
	basebundle.BaseController
}

//GetAll Retrieves all clients and send them as JSON
func (c *ClientController) GetAll(res http.ResponseWriter, req *http.Request) {

	db := store.GetDB()

	list := []Client{}

	db.Set("gorm:auto_preload", true).Find(&list)

	bytes, err := json.Marshal(&list)

	if err != nil {
		panic(err.Error())
	}

	res.Write(bytes)

	defer db.Close()
}

//GetByID handles /clients/{id:[0-9]+} and returns client with ID = id
func (c *ClientController) GetByID(res http.ResponseWriter, req *http.Request) {
	// vars := mux.Vars(req)
	// id := vars["id"]
	// value := "You are using GET method on /clients/" + id
	// json.NewEncoder(res).Encode(value)
}

func (c *ClientController) Create(res http.ResponseWriter, req *http.Request) {
	db := store.GetDB()

	client := Client{}

	err := json.NewDecoder(req.Body).Decode(&client)

	if err != nil {
		panic(err.Error())
	}

	now := time.Now()

	client.Suivi = now.AddDate(0, 0, 7*client.Frequence)
	db.Create(&client)

	res.WriteHeader(http.StatusCreated)

	defer db.Close()
}

func (c *ClientController) Update(res http.ResponseWriter, req *http.Request) {
	// json.NewEncoder(res).Encode("You are using PATCH method on /clients")
}

func (c *ClientController) Done(res http.ResponseWriter, req *http.Request) {
	param := c.ParseURL(req, "id")

	db := store.GetDB()

	id64, err := strconv.ParseInt(param, 10, 64)

	if err != nil {
		panic(err.Error())
	}
	id := int(id64)

	client := Client{}

	db.First(&client, Client{ID: id})

	fmt.Print(client)
	now := time.Now()
	client.Suivi = now.AddDate(0, 0, 7*client.Frequence)

	db.Save(&client)

	body, err := json.Marshal(client)

	if err != nil {
		panic(err.Error())
	}

	res.Write(body)

	defer db.Close()

}
