package staticbundle

import (
	"net/http"

	"bitbucket.org/gab_goffart/gesclient/bundles/basebundle"
)

type StaticController struct {
	basebundle.BaseController
}

func (s *StaticController) IndexHandler(res http.ResponseWriter, req *http.Request) {
	http.ServeFile(res, req, "./static/index.html")
}

func (s *StaticController) AjouterHandler(res http.ResponseWriter, req *http.Request) {
	http.ServeFile(res, req, "./static/ajouter.html")
}
