package basebundle

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

//BaseController Controller with basic functions for all other controllers
type BaseController struct {
}

//SendJSON Responds to the request with the interface in JSON
func (b *BaseController) SendJSON(res http.ResponseWriter, req *http.Request, v interface{}) {

	res.Header().Add("Access-Control-Allow-Origin", "*")

	if v == nil {
		res.WriteHeader(http.StatusNotFound)
		return
	}

	bts, err := json.Marshal(v)

	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		panic("Error writing the string to the response\n" + err.Error())
	} else {
		res.Write(bts)
	}
}

//ParseURL Parses url to find named parameter
func (b *BaseController) ParseURL(req *http.Request, name string) string {
	vars := mux.Vars(req)
	param := vars[name]

	if param == "" {
		panic("parameter " + name + " could not be found in URL")
	}

	return param
}
