package main

import (
	"fmt"
	"bitbucket.org/gab_goffart/gesclient/bundles/clientbundle"
	"bitbucket.org/gab_goffart/gesclient/bundles/statusclientbundle"
	"bitbucket.org/gab_goffart/gesclient/bundles/typeclientbundle"
	"bitbucket.org/gab_goffart/gesclient/store"
)

func createDB() {

	db := store.GetDB()

	db.AutoMigrate(clientbundle.Client{}, statusclientbundle.StatusClient{}, typeclientbundle.TypeClient{})

	client := typeclientbundle.TypeClient{}
	db.Create(&typeclientbundle.TypeClient{Type: "ACHETEUR"})

	db.First(&client, &typeclientbundle.TypeClient{Type: "ACHETEUR"})

	db.Create(&statusclientbundle.StatusClient{Status: "COLD", TypeClientID: client.ID})
	db.Create(&statusclientbundle.StatusClient{Status: "WARM", TypeClientID: client.ID})
	db.Create(&statusclientbundle.StatusClient{Status: "HOT", TypeClientID: client.ID})

	client = typeclientbundle.TypeClient{}

	db.Create(&typeclientbundle.TypeClient{Type: "VENDEUR"})

	db.First(&client, &typeclientbundle.TypeClient{Type: "VENDEUR"})

	db.Create(&statusclientbundle.StatusClient{Status: "AVPP", TypeClientID: client.ID})
	db.Create(&statusclientbundle.StatusClient{Status: "EXPIRÉ", TypeClientID: client.ID})
	db.Create(&statusclientbundle.StatusClient{Status: "HORS-MARCHÉ", TypeClientID: client.ID})
	db.Create(&statusclientbundle.StatusClient{Status: "LISTING", TypeClientID: client.ID})


	fmt.Println("migration complete")

	defer db.Close()
}
