package main

import (
	"net/http"

	"bitbucket.org/gab_goffart/gesclient/bundles/clientbundle"
	"bitbucket.org/gab_goffart/gesclient/bundles/staticbundle"
	"bitbucket.org/gab_goffart/gesclient/bundles/statusclientbundle"
	"bitbucket.org/gab_goffart/gesclient/bundles/typeclientbundle"
	"github.com/gorilla/mux"
)

func handleRoutes() *mux.Router {
	router := mux.NewRouter()

	cc := &clientbundle.ClientController{}
	scc := &statusclientbundle.StatusClientController{}
	tcc := &typeclientbundle.TypeClientController{}
	sc := &staticbundle.StaticController{}

	router.HandleFunc("/clients", cc.GetAll).Methods("GET")
	router.HandleFunc("/clients", cc.Create).Methods("POST")
	router.HandleFunc("/clients/{id:[0-9]+}", cc.GetByID).Methods("GET")
	router.HandleFunc("/clients/{id:[0-9]+}", cc.Update).Methods("PATCH")
	router.HandleFunc("/clients/{id:[0-9]+}/done", cc.Done).Methods("GET")

	router.HandleFunc("/types/{id:[0-9]+}/status", scc.GetByTypeClient).Methods("GET")
	router.HandleFunc("/status/{id:[0-9]+}", scc.GetByID).Methods("GET")

	router.HandleFunc("/types", tcc.GetAll).Methods("GET")
	router.HandleFunc("/types/{id:[0-9]+}", tcc.GetByID).Methods("GET")

	router.HandleFunc("/", sc.IndexHandler).Methods("GET")
	router.HandleFunc("/ajouter", sc.AjouterHandler).Methods("GET")

	router.PathPrefix("/static").Handler(http.StripPrefix("/static", http.FileServer(http.Dir("./static"))))

	return router
}
