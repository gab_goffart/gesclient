const url = "http://localhost:8080"

var $ = document.querySelector.bind(document)

document.addEventListener('DOMContentLoaded', () => {

	let elems = document.querySelectorAll('.sidenav');
	let instances = M.Sidenav.init(elems, {edge: "right"});
	
	// let table = $("table#clients")
	// let options = {
	// 	sortable: true
	// }

	// let dataTable = new DataTable(table, options)
	LoadClients()
	
})

function LoadClients() {
	fetch(url + "/clients").then(res => res.json()).then(clients => {

		let html = ""

		clients.forEach((x, index) => {

			/*
			<div class="card">
				<div class="card-content">
					<span class="card-title activator grey-text text-darken-4">Card Title<i class="material-icons right">more_vert</i></span>
					<p><a href="#">This is a link</a></p>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
					<p>Here is some more information about this product that is only revealed once clicked on.</p>
				</div>
			</div>
            */
        	let card = ""
        	card += "<div class='col l4 m6 s12'>"
        	card += 	"<div class='card small grey darken-4 white-text'>"
        	card += 		"<div class='card-content'>"
        	card += 			"<span class='card-title activator'>" + x.prenom + " " + x.nom + "<i class='material-icons right'>more_vert</i></span>"
        	card += 			"<ul>"
        	card += 				"<li> Téléphone : " + x.telephone + "</li>"
        	card += 				"<li> Courriel : " + x.courriel + "</li>"
        	card += 				"<li> Prochain suivi : " + x.suivi.split("T")[0] + "</li>"
        	card += 			"</ul>"
        	card += 		"</div>"
        	card +=			"<div class='card-action'>"
        	card += 			"<a href='#' class='btn-flat waves-effect waves-light teal-text'> Suivi terminé </a>"
        	card += 			"<a href='#' class='btn-flat waves-effect waves-light red-text'> Supprimer</a>"
        	card += 		"</div>"
        	card += 	"</div>"
        	card += "</div>"

			html+=card
		})
		$("#clients").innerHTML = html

	})
}
