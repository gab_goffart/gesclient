const url = "http://localhost:8080"
var $ = document.querySelector.bind(document)
class Client {
    constructor() {
        this.id = 0
        this.prenom = ""
        this.nom = ""
        this.courriel = ""
        this.telephone = ""
        this.adresse = ""
        this.commentaire = ""
        this.frequence = 0
        this.typeclientid = 0
        this.statusclientid = 0
    }
}

document.addEventListener('DOMContentLoaded', function () {

    let elems = document.querySelectorAll('.sidenav');
    let instances = M.Sidenav.init(elems, {edge: "right"});

    M.FormSelect.init($("select#statusclientid"))
    M.FormSelect.init($("select#typeclientid"))


    //M.AutoInit()
    fetch(url + "/types").then(res => {
        res = res.json().then(types => {

            $("select#statusclientid").innerHTML = ""
            let option = new Option("Veuillez choisir une option", 0, true)
            $("select#typeclientid").appendChild(option)
            types.forEach((x, index) => {
                let option = new Option(x.type, x.id)
                $("select#typeclientid").appendChild(option)
            })

            M.FormSelect.init($("select#typeclientid"))
        })
    })

    $("select#typeclientid").addEventListener("change", e => {

        let id = $("form #typeclientid").value
        console.log(id)
        fetch(url + "/types/" + id + "/status").then(res => {
            res = res.json().then(status => {

                $("select#statusclientid").innerHTML = ""
                let option = new Option("veuillez choisir une option", 0, true)
                $("select#statusclientid").appendChild(option)
                status.forEach((x, index) => {
                    let option = new Option(x.status, x.id)
                    $("select#statusclientid").appendChild(option)
                })

                M.FormSelect.init($("select#statusclientid"))
            })
        })
    })


    $("form#client").addEventListener("submit", function (e) {
        e.preventDefault()

        let client = new Client()

        client.nom = $("form #nom").value
        client.prenom = $("form #prenom").value
        client.courriel = $("form #courriel").value
        client.telephone = $("form #telephone").value
        client.adresse = $("form #adresse").value
        client.commentaire = $("form #commentaire").value
        client.frequence = Number($("form #frequence").value)
        client.typeclientid = Number($("form #typeclientid").value)
        client.statusclientid = Number($("form #statusclientid").value)

        if(client.typeclientid == 0 || client.statusclientid == 0) {
            return
        }

        fetch(url + "/clients",  {
            method:"POST",
            body: JSON.stringify(client)
        }).then(response => {
            console.log("The client has been created")
        })
        console.log(client);
        console.log("form has been submitted")

    })
})




