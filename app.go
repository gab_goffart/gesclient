package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {

	port := ":8080"

	createDB()

	router := handleRoutes()

	fmt.Println("Server Listenning on port ", port)

	log.Fatal(http.ListenAndServe(port, router))

}
