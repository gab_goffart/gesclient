package store

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql" //for gorm.Open
)

//GetDB returns an instance of a DB connection
func GetDB() *gorm.DB {
	db, err := gorm.Open("mysql", "root:@/gesclients?charset=utf8&parseTime=True&loc=Local")

	if err != nil {
		panic("Error connecting to the database\n" + err.Error())
	}

	return db
}
